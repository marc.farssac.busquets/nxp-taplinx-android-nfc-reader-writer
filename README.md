# NFC reader for NXP TapLinx and Android SDK

It allows to read and write NFC Tags of different types and technologies.

## About

On one side the android.nfc.tech of the Android SDK allows to access MifareClassic, MifareUltralight, Ndef, NdefFormatable, NfcA, NfcB, NfcBarcode, NfcF and NfcV Tag' properties and I/O operations. 
On the other side, the TapLinx SDK is the new Mifare SDK, an open API designed to ease the development of NFC based Apps, allowing to access all features of NXP's product portofolio, MIFARE® products, NTAG® and ICODE® products.

```
TapLinx SDK can be downloaded from [here](https://www.mifare.net/en/products/tools/taplinx/). More info can be found [here](https://www.mifare.net/wp-content/uploads/2016/10/TapLinx-leaflet-Ltr-LR.pdf)
The Android SDK can be downloaded from [here](https://developer.android.com/studio). More info can be found [here](https://developer.android.com/reference/android/nfc/tech/package-summary)
```

### Project status

Project is documented in [Jira](https://smog.atlassian.net/jira/software/projects/NFC/boards/4/backlog) and developed using an Agile approach, adding features and fixing bugs as git-flow branches as per the Jira workflow.
Currently it is a basic TapLinx App with Android SDK functionality.

![nxp-android-nfc-setup](/uploads/824497dc3d1e1699762a4bac575e3773/nxp-android-nfc-setup.jpeg)

## Built With

* [Jira](https://www.atlassian.com/software/jira) - The #1 software development tool used by agile teams
* [Android](https://developer.android.com/studio) - The fastest tools for building apps on every type of Android device.
* [Kotlin](https://kotlinlang.org/) - A cross-platform, statically typed, general-purpose programming language

## Authors

* **Marc Farssac** - *Initial combined work* - [NFC extended Project](https://gitlab.com/marc.farssac.busquets/nsp-and-ndef-near-field-communication-reader)
* **TapLinx developer community** - *Initial TapLinx work* - [Developer community](https://www.mifare.net/developer/home/?loggedin=true)

## License

This project is licensed under the MIT License 

## Acknowledgments

* TapLinx, NXP, Stackoverflow, Android developers